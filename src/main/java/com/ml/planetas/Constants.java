package com.ml.planetas;

/**
 * Constants
 * 
 * @author Jeremy Bacher
 *
 * May 27, 2019
 */
public class Constants {	
	public static double PI = Math.PI;	
	public static double tolercaciaEntreDoubles = 1.0e-10;
	public static Integer nanios = 10;
}