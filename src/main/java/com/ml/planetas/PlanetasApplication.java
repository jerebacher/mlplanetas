package com.ml.planetas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ml.planetas.bos.IClimaBO;
import com.ml.planetas.bos.impl.ClimaBO;

@SpringBootApplication
public class PlanetasApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(PlanetasApplication.class, args);
		
		IClimaBO climaBO = new ClimaBO();
		climaBO.calcularFenomenos();
	    
	    System.out.println("La cantidad de periodos de sequia seran: " + climaBO.devolverCantSequias());
	    System.out.println("La cantidad de periodos de lluvias seran: " + climaBO.devolverCantLluviasYPicoMax());
	    System.out.println("La cantidad de periodos de condiciones optimas de presion y temperatura seran: " + climaBO.devolverCantCondOptPresYTemp());
	    System.out.println("El dia 566 hay: " + climaBO.devolverFenomeno(566));
	}

}
