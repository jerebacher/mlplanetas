package com.ml.planetas.bos;

import java.util.HashMap;

import com.ml.planetas.recursos.estructura.CoordenadaCartesiana;
import com.ml.planetas.recursos.estructura.CoordenadaPolar;

/**
 * IClimaBO
 * 
 * @author Jeremy Bacher
 *
 * May 27, 2019
 */
public interface IClimaBO {
	
	public double anguloPlanetaSol(double velocidad, double tiempo);
	public double distancia(CoordenadaCartesiana coordenadaCartesiana1, CoordenadaCartesiana coordenadaCartesiana2);
	public double perimetro(CoordenadaCartesiana coordenadaCartesiana1, CoordenadaCartesiana coordenadaCartesiana2, CoordenadaCartesiana coordenadaCartesiana3);
	public boolean double_esigual(double dou1, double dou2);
	public boolean alineados_ang(double angulo1, double angulo2);
	
	public double prodVectorialPlaneta(CoordenadaCartesiana coordenadaCartesiana1, CoordenadaCartesiana coordenadaCartesiana2, CoordenadaCartesiana coordenadaCartesiana3);
	public boolean alineados_pos(CoordenadaCartesiana coordenadaCartesiana1, CoordenadaCartesiana coordenadaCartesiana2, CoordenadaCartesiana coordenadaCartesiana3);
	public boolean encuentra_maximo(double actual, double anterior, double anterior2);
	public CoordenadaCartesiana pasarPolarACartesiana(CoordenadaPolar coordenadaPolar);
	
	public void calcularFenomenos();
	public String devolverFenomeno(Integer dia);
	public Integer devolverCantSequias();
	public String devolverCantLluviasYPicoMax();
	public Integer devolverCantCondOptPresYTemp();
	public HashMap<Integer, String> devolverFenomenos();
}