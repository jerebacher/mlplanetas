package com.ml.planetas.bos.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.ml.planetas.Constants;
import com.ml.planetas.bos.IClimaBO;
import com.ml.planetas.recursos.Planetas;
import com.ml.planetas.recursos.estructura.CoordenadaCartesiana;
import com.ml.planetas.recursos.estructura.CoordenadaPolar;
import com.ml.planetas.recursos.impl.Betasoides;
import com.ml.planetas.recursos.impl.Ferengis;
import com.ml.planetas.recursos.impl.Vulcanos;

/**
 * ClimaBO
 * 
 * @author Jeremy Bacher
 *
 * May 27, 2019
 */
public class ClimaBO implements IClimaBO {

	private Map<Integer, String> diasFenomenos = new HashMap<Integer, String>();
	private Integer cantSequias;
	private Integer cantLluvias;
	private Integer cantCondOptPresYTemp;
	private Integer picoMaxLluvia;
	
	/*
	 * \param velocidad Velocidad angular, en grados/dia
	 * \param tiempo Tiempo, en dias
	 */
	@Override
	public double anguloPlanetaSol(double velocidad, double tiempo) {
		return velocidad * tiempo;
	}

	/*
	 * Distancia entre 2 astros
	 */
	@Override
	public double distancia(CoordenadaCartesiana coordenadaCartesiana1, CoordenadaCartesiana coordenadaCartesiana2) {
		return Math.sqrt( (coordenadaCartesiana1.getX() - coordenadaCartesiana2.getX()) * (coordenadaCartesiana1.getX() - coordenadaCartesiana2.getX()) + (coordenadaCartesiana1.getY() - coordenadaCartesiana2.getY()) * (coordenadaCartesiana1.getY() - coordenadaCartesiana2.getY()) );
	}

	/*
	 * Perimetro del triangulo formado por 3 astros
	 */
	@Override
	public double perimetro(CoordenadaCartesiana coordenadaCartesiana1, CoordenadaCartesiana coordenadaCartesiana2,
			CoordenadaCartesiana coordenadaCartesiana3) {
		return distancia(coordenadaCartesiana1, coordenadaCartesiana2) + distancia(coordenadaCartesiana2, coordenadaCartesiana3) + distancia(coordenadaCartesiana3, coordenadaCartesiana1);
	}

	/*
	 * Determina si dos variables de tipo double son iguales, dentro de cierta tolerancia
	 */
	@Override
	public boolean double_esigual(double dou1, double dou2) {
		return (Math.abs(dou1 - dou2) < Constants.tolercaciaEntreDoubles);
	}

	/*
	 * Determina por angulos si 2 planetas estan alineados con el sol
	 */
	@Override
	public boolean alineados_ang(double angulo1, double angulo2) {
		double resto = (angulo1 - angulo2) % 180;
	    return (double_esigual(resto, 0));
	}

	/*
	 * Producto vectorial entre los vectores entre (1,2) y (1,3)
	 * A 0 o 180 grados, da cero. Cuando cruza 0 o 180 grados cambia de signo.
	 */
	@Override
	public double prodVectorialPlaneta(CoordenadaCartesiana coordenadaCartesiana1,
			CoordenadaCartesiana coordenadaCartesiana2, CoordenadaCartesiana coordenadaCartesiana3) {
		return (coordenadaCartesiana2.getX()-coordenadaCartesiana1.getX()) * (coordenadaCartesiana3.getY()-coordenadaCartesiana1.getY()) - (coordenadaCartesiana2.getY()-coordenadaCartesiana1.getY()) * (coordenadaCartesiana3.getX()-coordenadaCartesiana1.getX());
	}
	
	/*
	 * Determina por posicion si 3 astros estan alineados, dentro de cierta tolerancia
	 */
	@Override
	public boolean alineados_pos(CoordenadaCartesiana coordenadaCartesiana1, CoordenadaCartesiana coordenadaCartesiana2,
			CoordenadaCartesiana coordenadaCartesiana3) {
		double prodVectorial = prodVectorialPlaneta(coordenadaCartesiana1, coordenadaCartesiana2, coordenadaCartesiana3);
	    return (double_esigual(prodVectorial, 0));
	}
	
	/*
	 * Detecta si hay un maximo de v, entre 3 pasos de tiempo
	 */
	@Override
	public boolean encuentra_maximo(double actual, double anterior, double anterior2) {
		return ( (anterior2 < anterior) && (anterior > actual) );
	}
	
	@Override
	public CoordenadaCartesiana pasarPolarACartesiana(CoordenadaPolar coordenadaPolar) {
		CoordenadaCartesiana coordenadadCartesiana = new CoordenadaCartesiana();
		double anguloRadio = coordenadaPolar.getAngulo() * Constants.PI / 180;
		coordenadadCartesiana.setX(coordenadaPolar.getRadio() * Math.cos(anguloRadio));
		coordenadadCartesiana.setY(coordenadaPolar.getRadio() * Math.sin(anguloRadio));
		return coordenadadCartesiana;
	}

	@Override
	public void calcularFenomenos() {
//		Datos
//		Velocidad angular, en grados/dia. Este en realidad no es necesario, porque es siempre cero
	    double velFerengis = 0.0;
//	    Velocidad angular, en grados/dia
	    double velVulcanos = 6.0;
//	    Velocidad angular, en grados/dia
	    double velBetasoides = -2.0;
	    double radioFerengis = 500;
	    double radioVulcanos = 1000;
	    double radioBetasoides = 2000;
	    Integer nanios = Constants.nanios;
	    
	    Planetas ferengis = new Ferengis();
	    Planetas vulcanos = new Vulcanos();
	    Planetas betasoides = new Betasoides();
	    // Calculos
	    Integer nsequia = 0;
	    Integer nptoptimos = 0;
	    Integer nlluvia = 0;
//	    Para contar los dias que vienen de años bisiestos
	    Integer ndias = nanios * 365 + (Integer)nanios / 4;
	    Integer dia = 0;

	    double prodVectorialAnt = 0;

	    double perimAnterior = 0;
	    double perimAnterior2 = 0;
	    boolean sequiaAnterior = false;
	    Map<Integer, Integer> diasMaximosLluvia = new HashMap<Integer, Integer>();

	    for (dia = 0; dia <= ndias; dia++) {
//	    	Este calculo en realidad no es necesario, porque es siempre cero
	        double anguloFerengi = this.anguloPlanetaSol(velFerengis, (double) dia);
	        double anguloVulcanos = this.anguloPlanetaSol(velVulcanos, (double) dia);
	        double anguloBetasoides = this.anguloPlanetaSol(velBetasoides, (double) dia);
//	        Este calculo en realidad no es necesario, porque es siempre {500, 0}
	        CoordenadaPolar coordPolarFerengi = new CoordenadaPolar(radioFerengis, anguloFerengi); 
	        ferengis.setCoordenadaPolar(coordPolarFerengi);
	        CoordenadaPolar coordPolarVulcanos = new CoordenadaPolar(radioVulcanos, anguloVulcanos);
	        vulcanos.setCoordenadaPolar(coordPolarVulcanos);
	        CoordenadaPolar coordPolarBetasoides = new CoordenadaPolar(radioBetasoides, anguloBetasoides);
	        betasoides.setCoordenadaPolar(coordPolarBetasoides);
//	        Este calculo en realidad no es necesario, porque es siempre {500, 0}
	        CoordenadaCartesiana coorCartFerengi = this.pasarPolarACartesiana(ferengis.getCoordenadaPolar());
	        ferengis.setCoordenadaCartesiana(coorCartFerengi);
	        CoordenadaCartesiana coorCartVulcanos = this.pasarPolarACartesiana(vulcanos.getCoordenadaPolar());
	        vulcanos.setCoordenadaCartesiana(coorCartVulcanos);
	        CoordenadaCartesiana coorCartBetasoides = this.pasarPolarACartesiana(betasoides.getCoordenadaPolar());
	        betasoides.setCoordenadaCartesiana(coorCartBetasoides);

	        // Verificar si (1,2,sol) y (1,3,sol) estan alineados
	        if ( this.alineados_ang(anguloFerengi, anguloVulcanos) && this.alineados_ang(anguloFerengi, anguloBetasoides) ) {
	            nsequia++;
//	            System.out.println("Comienzo dia " + dia + ", Sequia nro. " + nsequia);
	            diasFenomenos.put(dia, "Sequia");
	            sequiaAnterior = true;
	        } else {
	            double prodVectorial = this.prodVectorialPlaneta(ferengis.getCoordenadaCartesiana(), vulcanos.getCoordenadaCartesiana(), betasoides.getCoordenadaCartesiana());
	            if ( !sequiaAnterior ) {
	                // Verificar si (1,2,3) estan alineados entre si, pero ya no con el sol
	                if ( this.alineados_pos(ferengis.getCoordenadaCartesiana(), vulcanos.getCoordenadaCartesiana(), betasoides.getCoordenadaCartesiana()) ) {
	                    nptoptimos++;
//	                    System.out.println("Comienzo dia " + dia + ", Condiciones optimas de presion y temperatura nro. " + nptoptimos);
	                    diasFenomenos.put(dia, "Cond optimas de presion y temp");
	                }
	                // Verificar si cruzaron una configuracion de alineacion entre si, pero ya no con el sol
	                if ( (prodVectorial * prodVectorialAnt) < 0 ) {
	                    nptoptimos++;
//	                    System.out.println("Comienzo dia " + dia + ", Condiciones optimas de presion y temperatura nro. " + nptoptimos + " en el dia anterior");
	                    diasFenomenos.put(dia, "Cond optimas de presion y temp");
	                }
	            }
	            prodVectorialAnt = prodVectorial;
	            sequiaAnterior = false;

	            // Verificar perimetro maximo. En este caso es cuando ocurre la primera variacion negativa (maximo local)
	            double perim = this.perimetro(ferengis.getCoordenadaCartesiana(), vulcanos.getCoordenadaCartesiana(), betasoides.getCoordenadaCartesiana());
	            if ( this.encuentra_maximo(perimAnterior2, perimAnterior, perim) ) {
	                nlluvia++;
//	                System.out.println("Comienzo dia " + dia + ", Condiciones de lluvia nro. " + nlluvia + " en el dia anterior o el anterior y el perim es: " + perim);
	                diasFenomenos.put(dia, "Lluvia");
	                int intPerim = (int) perim;
	                Integer valPerim = Integer.valueOf(intPerim);
	                diasMaximosLluvia.put(dia, valPerim);
	            }
	            perimAnterior2 = perimAnterior;
	            perimAnterior = perim;
	        }
	    }
	    this.setCantSequias(nsequia);
	    this.setCantLluvias(nlluvia);
	    this.setCantCondOptPresYTemp(nptoptimos);
	    Integer maxValueInMap=(Collections.max(diasMaximosLluvia.values()));
	    Integer maxKey = -1;
	    for (Entry<Integer, Integer> entry : diasMaximosLluvia.entrySet()) {
            if (entry.getValue() == maxValueInMap) {
            	maxKey = entry.getKey();
            }
        }
	    this.setPicoMaxLluvia(maxKey);
	}

	public Integer getCantSequias() {
		return cantSequias;
	}

	public void setCantSequias(Integer cantSequias) {
		this.cantSequias = cantSequias;
	}

	public Integer getCantLluvias() {
		return cantLluvias;
	}

	public void setCantLluvias(Integer cantLluvias) {
		this.cantLluvias = cantLluvias;
	}

	public Integer getCantCondOptPresYTemp() {
		return cantCondOptPresYTemp;
	}

	public void setCantCondOptPresYTemp(Integer cantCondOptPresYTemp) {
		this.cantCondOptPresYTemp = cantCondOptPresYTemp;
	}

	@Override
	public String devolverFenomeno(Integer dia) {
		return diasFenomenos.get(dia);
	}

	@Override
	public Integer devolverCantSequias() {
		return this.getCantSequias();
	}

	@Override
	public String devolverCantLluviasYPicoMax() {
		return this.getCantLluvias() + " y el pico máximo de lluvia es: " + this.getPicoMaxLluvia();
	}

	@Override
	public Integer devolverCantCondOptPresYTemp() {
		return this.getCantCondOptPresYTemp();
	}

	public Integer getPicoMaxLluvia() {
		return picoMaxLluvia;
	}

	public void setPicoMaxLluvia(Integer picoMaxLluvia) {
		this.picoMaxLluvia = picoMaxLluvia;
	}  
	
	public Map<Integer, String> getDiasFenomenos() {
		return diasFenomenos;
	}

	public void setDiasFenomenos(Map<Integer, String> diasFenomenos) {
		this.diasFenomenos = diasFenomenos;
	}

	@Override
	public HashMap<Integer, String> devolverFenomenos() {
		return (HashMap<Integer, String>) this.getDiasFenomenos();
	}
  
}
