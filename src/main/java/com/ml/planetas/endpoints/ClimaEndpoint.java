package com.ml.planetas.endpoints;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.ml.planetas.services.IClimaService;
import com.ml.planetas.services.impl.ClimaService;

/**
 * ClimaEndpoint
 * 
 * @author Jeremy Bacher
 *
 * May 27, 2019
 */
@RestController
@WebServlet(name = "Clima", value="/clima")
public class ClimaEndpoint extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public @ResponseBody void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Integer dia = Integer.valueOf(req.getParameter("dia"));	
		IClimaService climaService = new ClimaService();
		String json = new Gson().toJson(climaService.getClima(dia));
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(json);
	}
}
