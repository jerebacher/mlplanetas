package com.ml.planetas.recursos;

import org.springframework.stereotype.Component;

import com.ml.planetas.recursos.estructura.CoordenadaCartesiana;
import com.ml.planetas.recursos.estructura.CoordenadaPolar;

/**
 * Planetas
 * 
 * @author Jeremy Bacher
 *
 * May 27, 2019
 */
@Component
public abstract class Planetas {
	
	private CoordenadaCartesiana coordenadaCartesiana;
	private CoordenadaPolar coordenadaPolar;

	public CoordenadaCartesiana getCoordenadaCartesiana() {
		return coordenadaCartesiana;
	}

	public void setCoordenadaCartesiana(CoordenadaCartesiana coordenadaCartesiana) {
		this.coordenadaCartesiana = coordenadaCartesiana;
	}

	public CoordenadaPolar getCoordenadaPolar() {
		return coordenadaPolar;
	}

	public void setCoordenadaPolar(CoordenadaPolar coordenadaPolar) {
		this.coordenadaPolar = coordenadaPolar;
	}

}
