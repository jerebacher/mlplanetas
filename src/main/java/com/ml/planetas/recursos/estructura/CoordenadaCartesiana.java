package com.ml.planetas.recursos.estructura;

/**
 * CoordenadaCartesiana
 * 
 * @author Jeremy Bacher
 *
 * May 27, 2019
 */
public class CoordenadaCartesiana {
	
	private double x;
	private double y;
	
	public double getX() {
		return x;
	}
	
	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
}
