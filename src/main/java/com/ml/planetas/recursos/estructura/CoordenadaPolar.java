package com.ml.planetas.recursos.estructura;

/**
 * CoordenadaPolar
 * 
 * @author Jeremy Bacher
 *
 * May 27, 2019
 */
public class CoordenadaPolar {
	
	private double radio;
	private double angulo;
	
	public CoordenadaPolar(double radio, double angulo) {
		super();
		this.radio = radio;
		this.angulo = angulo;
	}
	
	public double getRadio() {
		return radio;
	}
	
	public void setRadio(double radio) {
		this.radio = radio;
	}

	public double getAngulo() {
		return angulo;
	}

	public void setAngulo(double angulo) {
		this.angulo = angulo;
	}	
}
