package com.ml.planetas.recursos.impl;

import org.springframework.stereotype.Component;

import com.ml.planetas.recursos.Planetas;

/**
 * Vulcanos
 * 
 * @author Jeremy Bacher
 *
 * May 27, 2019
 */
@Component(value="Vulcanos")
public class Vulcanos extends Planetas {
	
}
