package com.ml.planetas.responses;

/**
 * Response
 * 
 * @author Jeremy Bacher
 *
 * May 27, 2019
 */
public abstract class Response {
//	Clase abstracta con lo que se va a devolver en todas los llamados al servicio 

	private Integer status;
	
    public Integer getStatus() {
		return status;
	}
    
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
