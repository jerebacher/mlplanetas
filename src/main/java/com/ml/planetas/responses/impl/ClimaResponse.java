package com.ml.planetas.responses.impl;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ml.planetas.responses.Response;

/**
 * ClimaResponse
 * 
 * @author Jeremy Bacher
 *
 * May 27, 2019
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClimaResponse extends Response {
	//Clase con el formato de respuesta del servicio
	
	private Integer dia;
    private String clima;
	
	public ClimaResponse(Integer dia, String clima) {
		super();
		this.dia = dia;
		this.clima = clima;
	}    
    
	public Integer getDia() {
		return dia;
	}
	
	public void setDia(Integer dia) {
		this.dia = dia;
	}

	public String getClima() {
		return clima;
	}

	public void setClima(String clima) {
		this.clima = clima;
	}
    
}