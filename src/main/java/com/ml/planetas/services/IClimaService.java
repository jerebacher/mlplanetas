package com.ml.planetas.services;

import java.util.HashMap;

import org.springframework.stereotype.Service;

import com.ml.planetas.responses.Response;

/**
 * IClimaService
 * 
 * @author Jeremy Bacher
 *
 * May 27, 2019
 */
@Service
public interface IClimaService {

	public Response getClima(Integer dia);
	
	public HashMap<Integer, String> init();
}
