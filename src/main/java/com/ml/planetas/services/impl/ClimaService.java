package com.ml.planetas.services.impl;

import java.util.HashMap;

import org.springframework.stereotype.Service;

import com.ml.planetas.bos.IClimaBO;
import com.ml.planetas.bos.impl.ClimaBO;
import com.ml.planetas.responses.Response;
import com.ml.planetas.responses.impl.ClimaResponse;
import com.ml.planetas.services.IClimaService;

/**
 * ClimaService
 * 
 * @author Jeremy Bacher
 *
 * May 27, 2019
 */
@Service(value="ClimaService")
public class ClimaService implements IClimaService {
	
	IClimaBO climaBO = new ClimaBO();
        
    public Response getClima(Integer dia) { 	
    	Response response = null;
    	climaBO.calcularFenomenos();
    	String clima = climaBO.devolverFenomeno(dia);
    	response = new ClimaResponse(dia, clima);    	
    	response.setStatus(200);
    	return response;
    }

	@Override
	public HashMap<Integer, String> init() {
		climaBO.calcularFenomenos();
		return climaBO.devolverFenomenos();
		
	}    
  
}
